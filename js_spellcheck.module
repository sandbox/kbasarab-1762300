<?php
/**
 * @file
 * Implements a jQuery spellcheck prior to form submission of node body in user
 * selected content types.
 */

/**
 * Implements hook_permssion().
 */
function js_spellcheck_permission() {
  return array(
    'configure spellcheck settings' => array(
      'title' => t('Configure spellcheck settings'),
      'description' => t('Allows user access to admin/config/spellcheck'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function js_spellcheck_menu() {
  $items = array();

  $items['admin/config/js_spellcheck'] = array(
    'title' => 'JS Spellcheck',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('js_spellcheck_admin'),
    'access arguments' => array('configure spellcheck settings'),
    'description' => 'Configure which content types to enable spellchecker on',
    'file path' => drupal_get_path('module', 'js_spellcheck') . '/includes',
    'file' => 'js_spellcheck.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Implements hook_form_alter().
 *
 * Applies spellcheck JS to node_form of content types specificed in admin menu.
 */
function js_spellcheck_form_alter(&$form, &$form_state, $form_id) {
  $content_types = variable_get('js_spellcheck_content_types', array());
  foreach ($content_types as $type) {
    if ($form_id == $type . '_node_form') {
      // Add $form_id and node language type to Drupal.settings.
      // Replaces $form_id underscores with hypens to match CSS classes on page.
      drupal_add_library('js_spellcheck', 'js_spellcheck', FALSE);
      $form['#attached']['js'][] = array(
        'data' => array(
          'js_spellcheck' => array(
            'formid' => str_replace('_', '-', $form_id),
            'body_language' => $form['body']['#language'],
            'dictionary' => variable_get('js_spellcheck_dictionary', 'English (International)'),
          ),
        ),
        'type' => 'setting',
      );
      // Adds the spellcheck.js file which does the heavy lifting.
      $form['#attached']['js'][] = array(
        'data' => drupal_get_path('module', 'js_spellcheck') . '/includes/js_spellcheck.js',
        'type' => 'file',
      );
      $form['#attached']['js'][] = array(
        'data' => libraries_get_path('js_spellcheck') . '/include.js',
        'type' => 'file',
      );
    };
  }
}

/**
 * Implements hook_library().
 */
function js_spellcheck_library() {
  $libraries['js_spellcheck'] = array(
    'title' => 'js Spellcheck',
    'website' => 'http://www.javascriptspellcheck.com/',
    'js' => array(
      libraries_get_path('js_spellcheck') . '/include.js' => array(),
    ),
    'version' => '1.0',
  );
  return $libraries;
}
