<?php
/**
 * @file
 * Admin settings form for js_spellcheck module.
 */

/**
 * Implements page callback for js_spellcheck_admin_page().
 */
function js_spellcheck_admin() {
  $content_types = node_type_get_names();
  $dictionaries = js_spellcheck_get_dictionaries();
  if (count($dictionaries) == 0) {
    drupal_set_message(t('No valid dictionaries found. Please ensure the JS Spellcheck library is installed.'), 'error');
  }

  $form = array();

  $form['js_spellcheck_content_types'] = array(
    '#title' => t('Content types to activate spellcheck on'),
    '#type' => 'checkboxes',
    '#attributes' => array(),
    '#title_display' => 'before',
    '#default_value' => variable_get('js_spellcheck_content_types', array()),
    '#options' => $content_types,
  );

  $form['js_spellcheck_dictionary'] = array(
    '#title' => t('Dictionary to use'),
    '#type' => 'radios',
    '#default_value' => variable_get('js_spellcheck_dictionary', 'English (International)'),
    '#options' => js_spellcheck_get_dictionaries(),
  );
  return system_settings_form($form);
}

/**
 * Searches library filesystem for dictionaries.
 *
 * Dictionaries can be downloaded from:
 * http://www.javascriptspellcheck.com/JavaScript_SpellChecking_Dictionaries
 */
function js_spellcheck_get_dictionaries() {
  $path = libraries_get_path('js_spellcheck');
  $files = file_scan_directory($path, '/.*\.dic$/');
  $dictionary = array();
  if (!is_array($files)) {
    return array();
  }
  foreach ($files as $file) {
    $dictionary[$file->name] = $file->name;
  }
  return $dictionary;
}
