(function ($) {
  Drupal.behaviors.js_spellcheck = {
    attach: function (context, settings) {
      var lang_id = Drupal.settings.js_spellcheck.body_language;
      if (typeof(lang_id) == 'undefined') {
        lang_id = 'und';
      }
      $('#' + Drupal.settings.js_spellcheck.formid + " #edit-submit:last").click(function() {
        if ($('.field-name-field-skip-spellcheck input:checked').length > 0) { return true; }
        // @TODO: Add GUI configuration for this selector.
        $("#edit-body-" + lang_id + "-0-value").spellCheckInDialog({
            submitFormById: Drupal.settings.js_spellcheck.formid,
            defaultDictionary: Drupal.settings.js_spellcheck.dictionary,
            theme: 'clean'
          });
        return false;
      });
    }
  }
})(jQuery);
