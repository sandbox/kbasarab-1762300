Authored by Kevin Basarab (@kbasarab)
http://drupal.org/user/336254
http://kbasarab.com

Integrates automated spellchecking in a popup window using the third party
service http://www.javascriptspellcheck.com/.

## Installation
1. Download and install module.
2. Download library from http://www.javascriptspellcheck.com/.
3. Extract contents into sites/all/libraries/js_spellcheck
4. Configure content types to enable spellchecking at: admin/config/spellcheck.

## Optional

**Install international dictionaries**
1. Download dictionary from
http://www.javascriptspellcheck.com/JavaScript_SpellChecking_Dictionaries
2. Extract .zip and place the .dic file into sites/all/libraries/js_spellcheck
3. Flush caches and choose new dictionary on admin/config/spellcheck

**Enable a bypass field**
Choose this option to skip spellchecking when submitting a node. This is useful
when debugging or developing other features for nodes.

1. Add field to node with machine name: "field_skip_spellcheck".
2. Field type is "Boolean"
3. Field widget as "Single on/off checkbox"

If field is checked on node submission spellchecking will be skipped.
See includes/spellcheck.js.
